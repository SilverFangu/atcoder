a = list(map(int, input().split()))
cnt_5 = a.count(5)
cnt_7 = a.count(7)

if cnt_5 == 2 and cnt_7 == 1:
    print('YES')
else:
    print('NO')
